var soundMap = {
	crystalon: ['assets/sound/crystalon.mp3', 'assets/sound/crystalon.ogg', 'assets/sound/crystalon.wav'],
	toohot: ['assets/sound/toohot.mp3', 'assets/sound/toohot.ogg', 'assets/sound/toohot.wav'],
	death: ['assets/sound/death.mp3', 'assets/sound/death.ogg', 'assets/sound/death.wav'],
	hit: ['assets/sound/hit.mp3', 'assets/sound/hit.ogg', 'assets/sound/hit.wav'],
	bgmusic: ['assets/sound/bgmusic.mp3', 'assets/sound/bgmusic.ogg', 'assets/sound/bgmusic.wav'],
	crystaloff: ['assets/sound/crystaloff.mp3', 'assets/sound/crystaloff.ogg', 'assets/sound/crystaloff.wav'],
	jump: ['assets/sound/jump.mp3', 'assets/sound/jump.ogg', 'assets/sound/jump.wav'],
	meteorite: ['assets/sound/meteorite.mp3', 'assets/sound/meteorite.ogg', 'assets/sound/meteorite.wav'],
	land: ['assets/sound/land.mp3', 'assets/sound/land.ogg', 'assets/sound/land.wav'],
	toocold: ['assets/sound/toocold.mp3', 'assets/sound/toocold.ogg', 'assets/sound/toocold.wav'],
};