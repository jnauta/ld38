var mutemusic = false;//false;
var mutesound = false;
var debug = false;//true;
var playing = false;
var fontFamily1  = "Ranga"; // info text
var fontFamily2 = "Cinzel"; // help text
var fontFamily3 = "Bungee Outline"; // title?

Crafty.paths({
	audio: 'assets/sound/',
	images: 'assets/images/'
});

Game = {
	width: 600,
	height: 600,
	crystalCounter: 0,
	tportCooldown: 0,
    
	start: function() {
		// Start crafty and set a background color so that we can see it's working
		Crafty.init(Game.width,Game.height, 'cr-stage');
    //Crafty.viewport.zoom(20,0,0,100);
    //Crafty.pixelart(true);
		Crafty.scene('Loading');
	},
};