utility = {
	deg2rad: Math.PI / 180,
	rad2deg: 180 / Math.PI,
	pol2car: function(r, phi) {
		return {
			x: Math.cos(phi) * r,
			y: Math.sin(phi) * r
		}
	},
	car2pol: function(x, y) {
		return {
			r: Math.sqrt(x*x + y*y),
			phi: Math.atan2(y, x)
		}
	},
	tiled2pol: function(col, row, arrayIndexed) {
		if (!arrayIndexed) {
			col = col / tileSize;
			row = level.height - row / tileSize;
		}
		return {
			r: row*params.layerThickness + params.surfaceRadius,
			phi: col*angleStepRad
		}
	},
	pol2tiledCR: function(r, phi) {
		phi = utility.normalizeAngle(phi);
		return {
			row: Math.floor((r - params.surfaceRadius) / params.layerThickness),
			col: Math.floor(phi / angleStepRad)
		}
	},
	normalizeAngle: function(phi) {
		while(phi < 0) {
			phi += 2 * Math.PI;
		}
		return phi % (2 * Math.PI);
	},
	// sign: function(num) {
	// 	if (num === 0) {
	// 		return 1;
	// 	}
	// 	return num / Math.abs(num);
	// }
};