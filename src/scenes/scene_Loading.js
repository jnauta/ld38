// LOADING SCENE
Crafty.scene('Loading', function() {
	// // Draw some text for the player to see in case the file
	// //  takes a noticeable amount of time to load

	//Crafty.background('url(assets/images/background1.png)');
    Crafty.background(mycolors.background);
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 220,
			y: 270,
			w: 200,
			h: 200,
			z: 8
		})
		.text('Loading...')
		.textFont({
			size: '34px',
			family: fontFamily1
		})
		.css({
			'text-align': 'center',			
			'color': '#000000'
			
		});
	Crafty.load(assetsObject,
		function() {
			//when loaded

			Crafty.sprite(30, 20, "assets/images/player.png", {
				player: [0,0],				
			});	

			Crafty.sprite(33, 20, "assets/images/sun.png", {
				sunny: [0,0],				
			});	

			Crafty.sprite(20, 20, "assets/images/fireball.png", {
				fireball: [0,0],				
			});

			Crafty.sprite(30, 30, "assets/images/cathopper.png", {
				cathopper: [0,0],				
			});	

			Crafty.sprite(30, 30, "assets/images/firesplat.png", {
				firesplat: [0,0],				
			});	

			Crafty.sprite(30, 30, "assets/images/wyrm.png", {
				wyrm: [0,0],				
			});	

			Crafty.sprite(30, 30, "assets/images/rumbleweed.png", {
				rumbleweed: [0,0],				
			});

			Crafty.sprite(30, 30, "assets/images/crystal.png", {
				crystal: [0,0],				
			});	


			Crafty.sprite(30, 30, "assets/images/savePoint.png", {
				savePoint: [0,0],				
			});	

			Crafty.sprite(150, 200, "assets/images/rocket.png", {
				rocket: [0,0],				
			});	

			Crafty.sprite(150, 150, "assets/images/meteorite.png", {
				meteorite: [0,0],				
			});	

			Crafty.sprite(180, 115, "assets/images/ufo.png", {
				ufo: [0,0],				
			});	

			Crafty.sprite(30, 20, "assets/images/teleporter.png", {
				teleporter: [0,0],				
			});	

			levelIdx = 0;
			Crafty.scene('Main');
		},

		function(e) { // onProgress
			// console.log(e.src);
		},

		function(e) {
			console.log('loading error');
			console.log(e.src);
		}
	);
    
    
    

});