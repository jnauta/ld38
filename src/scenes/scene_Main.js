var overlay;
var bgMusic = null;
var player = null;
var respawnPos = {r:0, phi:0};

// MAIN SCENE
Crafty.scene('Main', function() {
	Crafty.timer.FPS(60);
	
	if (!bgMusic) {
		bgMusic = Crafty.audio.play('bgMusic',-1,0.3);
		if(mutemusic && bgMusic.source){
			bgMusic.pause();
		}
	}

	infoDiv = document.getElementById('info');
	infoDiv.innerHTML = "Press Space to start";
	infoDiv.style.fontFamily = fontFamily1;
	// workaround for infoupdate, such that delays wont make if invisible too soon
	infoCount = 0;
	infoUpdateText = function(text,time){		
		if(text != infoDiv.innerHTML || infoDiv.style.visibility === 'hidden'){
			infoCount += 1;
			// console.log(infoCount, text);
			infoDiv.style.visibility = "visible";
			infoDiv.innerHTML = text;
			Crafty.e('Delay').delay(function(){
				infoCount -= 1;
				// console.log(infoCount);
				if(infoCount === 0){
					infoDiv.style.visibility = "hidden";
					
					this.destroy();
				}
			},time,0);
		}
	};

	Crafty.createLayer("SunLayer", "Canvas", {scaleResponse: 0, xResponse: 0, yResponse: 0, z:11000})

	
	buildLevel = function(resetting){
		if (!resetting) {
			level = TileMaps["level1"];
			var tileSets = level.tilesets;
			var xTiles, yTiles;
			for (var i = tileSets.length - 1; i >= 0; i--) {
				var t = tileSets[i];
				if (t.name === "tiles") {
					// these are the real tiles; solid stuff, background etc.
					tileSize = t.tilewidth;
					xTiles = t.imagewidth / t.tilewidth;
					yTiles = t.imageheight / t.tileheight;
				}
			};
			
			angleStepDeg = 360/level.width;
			angleStepRad = Math.PI*2/level.width;
			
			
			solidLayer = null;
			bgLayer = null;
			hopcatLayer = null;
			rumbleweedLayer = null;
			iwLayer = null;
			iceLayer = null;
			pLayer = null;
			areaLayer = null;
			asteroidIdxs = new Array();
			saveLayer = null;

			for (var i = level.layers.length - 1; i >= 0; i--) {
				var layer = level.layers[i];
				if (layer.name === "solid") {
					solidLayer = layer;
				} else if (layer.name === "background") {
					bgLayer = layer;
				} else if (layer.name === "hopcats") {
					hopcatLayer = layer;
				} else if (layer.name === "rumbleweeds") {
					rumbleweedLayer = layer;
				} else if (layer.name === "icewyrms") {
					iwLayer = layer;
				} else if (layer.name === "crystals") {
					iceLayer = layer;
				} else if (layer.name === "firewyrms") {
					fwLayer = layer;
				} else if (layer.name === "player") {
					pLayer = layer;
				} else if (layer.name === "areas") {
					areaLayer = layer;
				} else if (layer.name === "saves") {
					saveLayer = layer;
				}
			}

			

			solidTileData = convertLevelTo2D(solidLayer.data, level.height);
			for (var row = 0; row < level.height; ++row) {
				for (var col = 0; col < level.width; ++col) {
					// create tile for platform layer
					var tileIdx = solidTileData[row][col];
					if (tileIdx === 1) {						
						Crafty.e('Solid')._Solid(col,row, "normal");
					}else if (tileIdx === 13) {						
						Crafty.e('Solid')._Solid(col,row, "painful");
					}else if (tileIdx === 6) {						
						//Crafty.e('Solid')._Solid(col,row, "monsterblock");
					}else if (tileIdx === 23) {						
						Crafty.e('Solid, Lava')._Solid(col,row, "lava");
					}else if (tileIdx === 15) {
						asteroidIdxs.push({col: col, row: row});
					}
				}
			}
			Crafty.e('Planet')._Planet(params.surfaceRadius);
		}
		// this happens also during reset
		Game.crystalCounter = 0;
		Game.tempBuff = 0;

		var pObj = pLayer.objects[0];
		playerPolars = utility.tiled2pol(pObj.x, pObj.y);
		if( respawnPos.r !=0){
			playerPolars =  {r:respawnPos.r, phi:respawnPos.phi};		
		}
		player = Crafty.e('Player').
			_KeyControls(Crafty.keys.LEFT_ARROW, Crafty.keys.RIGHT_ARROW, Crafty.keys.UP_ARROW, Crafty.keys.DOWN_ARROW).	
			_Player(playerPolars.r, playerPolars.phi, params.playerAcc);		

		// need coordinates later:
		rocketObj = pLayer.objects[1];
		rocketPolars = utility.tiled2pol(rocketObj.x, rocketObj.y);

		meteorObj = pLayer.objects[2];
		meteorPolars = utility.tiled2pol(meteorObj.x, meteorObj.y);

		ufoObj = pLayer.objects[3];
		ufoPolars = utility.tiled2pol(ufoObj.x, ufoObj.y);


		ufo = Crafty.e('2D, Canvas, ufo, Tween, Polar')
			._Polar(ufoPolars.r, ufoPolars.phi).attr({z:params.zLayers['player'] -1,}).origin('center');

		ufo.updateXY();
		ufo.rotation -= 90;
		sunShine = Crafty.e('Sun')._Sun();
		
		if( respawnPos.r !=0){
			player.temp =  Math.max(Math.min(respawnPos.playerTemp, params.chillMaxTemp),params.chillMinTemp);
			sunShine.time = respawnPos.sunTime;
		}	
		sunShine.updateTime();

		var hopcatObjs = hopcatLayer.objects;
		hopcats = [];
		for (var hopcatIdx = 0; hopcatIdx < hopcatObjs.length; ++hopcatIdx) {
			var hopcat = hopcatObjs[hopcatIdx];

			var hopcatPolars = utility.tiled2pol(hopcat.x, hopcat.y);
			hopcats.push(Crafty.e('Hopcat')._Hopcat(hopcatPolars.r, hopcatPolars.phi, hopcat.properties.speed));
		}

		var rumbleweedObjs = rumbleweedLayer.objects;
		rumbleweeds = [];
		for (var rumbleweedIdx = 0; rumbleweedIdx < rumbleweedObjs.length; ++rumbleweedIdx) {
			var rumbleweed = rumbleweedObjs[rumbleweedIdx];
			var rumbleweedPolars = utility.tiled2pol(rumbleweed.x, rumbleweed.y);
			rumbleweeds.push(Crafty.e('Rumbleweed')._Rumbleweed(rumbleweedPolars.r, rumbleweedPolars.phi, rumbleweed.properties.speed));
		}

		var iwObjs = iwLayer.objects;
		icewyrms = [];
		for (var iwIdx = 0; iwIdx < iwObjs.length; ++iwIdx) {
			var iw = iwObjs[iwIdx];
			var iwPolars = utility.tiled2pol(iw.x, iw.y);
			icewyrms.push(Crafty.e('Wyrm')._Wyrm(iwPolars.r, iwPolars.phi, iw.properties.speed, "wyrm"));//iw.properties.kind));
		}

		var fwObjs = fwLayer.objects;
		firewyrms = [];
		spawners = [];
		for (var fwIdx = 0; fwIdx < fwObjs.length; ++fwIdx) {
			var fw = fwObjs[fwIdx];
			var fwPolars = utility.tiled2pol(fw.x, fw.y);
			if (fw.name === "spawner") {
				spawner1 = Crafty.e("Spawner")._Spawner(fwPolars.r, fwPolars.phi, fw.properties.maxNum, fw.properties.speed);
				// spawner1.startSpawn(180);
				spawners.push(spawner1);
			} else if (fw.name === "spawner2") {
				spawner2 = Crafty.e("Spawner")._Spawner(fwPolars.r, fwPolars.phi, fw.properties.maxNum, fw.properties.speed);
				spawners.push(spawner2);
			}
		}

		var iceObjs = iceLayer.objects;
		crystals = [];
		iceRooms = [];
		for (var iceIdx = 0; iceIdx < iceObjs.length; ++iceIdx) {
			var ice = iceObjs[iceIdx];
			var icePolars = utility.tiled2pol(ice.x, ice.y);
			if (ice.name === "crystal") {
				var row = level.height - ice.y / tileSize;
				var col = ice.x / tileSize;
				crystals.push(Crafty.e('Crystal')._Crystal(icePolars.r,icePolars.phi, ice.properties.idx));
			} else if (ice.name === "room") {
				// calculate center of room 

				var iceRoom = Crafty.e("2D, Canvas, Collision, Shelter, Polar")._Polar(icePolars.r, icePolars.phi).
					_Shelter(0, "average").
					attr({w: 2 * Math.PI / (level.width * tileSize) * icePolars.r * ice.width,
								h: params.layerThickness * ice.height / tileSize, rotation: utility.rad2deg * icePolars.phi + 90,
								T: ice.properties.T});
				iceRooms.push(iceRoom);
			}
		};

		var areaObjs = areaLayer.objects;
		areas = [];

		for (var areaIdx = 0; areaIdx < areaObjs.length; ++areaIdx) {
			var areaObj = areaObjs[areaIdx];
			var areaPolars = utility.tiled2pol(areaObj.x, areaObj.y);

			var area = Crafty.e("2D, Canvas, Area, Collision, Polar, ")
			._Polar(areaPolars.r, areaPolars.phi).
				attr({w: 2 * Math.PI / (level.width * tileSize) * areaPolars.r * areaObj.width,
							h: params.layerThickness * areaObj.height / tileSize, rotation: utility.rad2deg * areaPolars.phi + 90});
			area.name = areaObj.name;
			areas.push(area);
		};
		
		var saveObjs = saveLayer.objects;
		saves = [];
		for (var j = 0; j < saveObjs.length; j++) {
			var saveObj = saveObjs[j];
			var savePolars = utility.tiled2pol(saveObj.x, saveObj.y);
			save = Crafty.e("Save, 2D, Canvas, savePoint, SpriteAnimation, Polar, Collision, ")
			._Polar(savePolars.r, savePolars.phi)
			.attr({rotation: utility.rad2deg * savePolars.phi + 90})			
			.reel("On",900,1,0,3).reel("Off",900, 0,0,1);
			

			saves.push(save);
		}



// BIND ALL ENTER FRAME FUNCTIONS
	
	};

	enterFrameFunction = function() {
		player.velocityUpdate();
		player.moveUpdate();
		Game.tportCooldown = Math.max(Game.tportCooldown - 1, 0);
		Game.tempBuff = Math.max(Game.tempBuff - 0.002, 0);

		
		// where else to put it...
		if(player.jumping && player.vr < 0){player.changeState('Fall'); }
	

		// player.checkWin();
		for (var i = 0; i < hopcats.length; i++) {
			hopcats[i].velocityUpdate();
			hopcats[i].moveUpdate();
		}
		for (var i = 0; i < rumbleweeds.length; i++) {
			rumbleweeds[i].velocityUpdate();
			rumbleweeds[i].moveUpdate();
		}
		for (var i = 0; i < icewyrms.length; i++) {
			icewyrms[i].velocityUpdate();
			icewyrms[i].moveUpdate();
		}
		for (var i = 0; i < firewyrms.length; i++) {
			firewyrms[i].velocityUpdate();
			firewyrms[i].moveUpdate();
		}
		for (var i = 0; i < spawners.length; i++) {
			spawners[i].spawn();
		}



		sunShine.updateTime();
		// console.log(sunShine.time);
		player.bodyHeatUpdate(sunShine.temp);

		sunShine.x = 290 + Math.cos(sunShine.time)*285;
		sunShine.y = 290 + Math.sin(sunShine.time)*285;
		
		Crafty.viewport.x = (Game.width / 2) - Math.cos(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);
		Crafty.viewport.y = (Game.height / 2) - Math.sin(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);
	};
	resetLevel = function() {
		Crafty.unbind("EnterFrame");
		//var hopcats = Crafty('Hopcat');
		for (var i = 0; i < hopcats.length; i++) {
			var hopcat = hopcats[i];
			hopcat.destroy();
		}
		//var rumbleweeds = Crafty('Rumbleweeds');
		for (var i = 0; i < rumbleweeds.length; i++) {
			var rumbleweed = rumbleweeds[i];
			rumbleweed.destroy();
		}
		for (var i = 0; i < crystals.length; i++) {
			var ice = crystals[i];
			ice.destroy();
		}
		for (var i = 0; i < iceRooms.length; i++) {
			var ice = iceRooms[i];
			ice.destroy();
		}
		for (var i = 0; i < icewyrms.length; i++) {
			var ice = icewyrms[i];
			ice.destroy();
		}
		for (var i = 0; i < firewyrms.length; i++) {
			var fire = firewyrms[i];
			fire.destroy();
		}
		for (var i = 0; i < spawners.length; i++) {
			var spawn = spawners[i];
			spawn.destroy();
		}
		for (var i = 0; i < areas.length; i++) {
			var area = areas[i];
			area.destroy();
		}
		for (var i = 0; i < saves.length; i++) {
			var save = saves[i];
			save.destroy();
		}



		tempBonus = Math.max(Game.tempBuff - 0.1, 0);

		sunShine.destroy();
		player.destroy();
		sunShine.destroy();	
		ufo.destroy();	
		buildLevel(true);
		Crafty.bind("EnterFrame", enterFrameFunction);
	}

	// build level, enemies etc
	buildLevel();
	
	// for intro, commment for debug
	player.visible = false;
	player.playing = false;
	player.radius = rocketPolars.r - 19;
	player.angle = rocketPolars.phi;
	sunShine.attr({alpha:0});

	// uncomment for debug
	// Crafty.bind("EnterFrame", enterFrameFunction);

	player.updateXY();
	Crafty.viewport.x = (Game.width / 2) - Math.cos(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);
	Crafty.viewport.y = (Game.height / 2) - Math.sin(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);


	Crafty('Solid').each(function(){
		// how to make sure that it is drawn properly?
	});

	Crafty.e('Keyboard').bind('KeyDown',function(){
		if(this.isDown('SPACE')){
			introFunction();
			this.destroy();
		}
	});

	introFunction = function(){

		
		
		infoDiv.style.visibility = 'hidden';

		rocket = Crafty.e('2D, Canvas,Polar, rocket,Tween,')
		.attr({z:params.zLayers['player'] -1, x:player.x-270, y:player.y-900, rotation : player.angle*utility.rad2deg+90})
		.origin(70,200)
		.tween({x:player.x-65, y:player.y-165,rotation: player.angle*utility.rad2deg+90},3000,'easeOutQuad');
		meteorite = Crafty.e('2D, Canvas, meteorite, Tween, Polar')._Polar(meteorPolars.r+600,meteorPolars.phi).attr({z:params.zLayers['player'] -1,});
		meteorite.updateXY();
				
		

		var introTime = 0;
		Crafty.bind('EnterFrame',function(){
			introTime += 1;
			player.updateXY();
			meteorite.updateXY();
			player.rotation =  player.angle*utility.rad2deg+90;
			Crafty.viewport.x = (Game.width / 2) - Math.cos(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);
			Crafty.viewport.y = (Game.height / 2) - Math.sin(player.angle) * ((params.surfaceRadius + player.radius) / 2 + 150);
			

			if(introTime === 30){
				Crafty.audio.play('land',1,1);

			}else if(introTime === 200){	
				player.visible = true;
			}else if(introTime === 230){
				infoDiv.style.visibility = "visible";
				infoDiv.innerHTML = "Landing sequence completed! Congratulations Commander.";

			}else if(introTime === 360){
				player.tween({angle: playerPolars.phi},5200);
				player.animate('Walk3',-1);
			}else if(introTime === 520){
				infoDiv.innerHTML ="Time to explore this planet.";			
			}else if(introTime === 610){
				Crafty.audio.play('meteorite',1,0.5);
			}else if(introTime === 690){
				// Crafty.e('2D, Canvas, meteorite,Tween').attr({z:params.zLayers['player'] -1, x:player.x-170, y:player.y-900,rotation: player.angle})
				meteorite.tween({radius: meteorPolars.r, angle: meteorPolars.phi},500,'easeOuQuad');

			}else if(introTime === 710){
				player.animate('Stand3',-1);
				player.flip('X');
			}else if(introTime === 800){	
				infoUpdateText('Oh dear - that\'s what happens to away teams...',3000);
			}else if( introTime === 1000){
				infoUpdateText("Since this is a 2D planet,<br>you have to take the long way around.",4000);
				Crafty.e('Delay').delay(function(){
					infoUpdateText("Try to stay in the twilight,<br>only there the temperature is bearable.",5000);
				},4000,0);
			
				
				Crafty.unbind('EnterFrame');
				player.playing = true; 
				// make asteroid pieces unpassable
				for (var i = 0; i < asteroidIdxs.length; i++) {
					var ast = asteroidIdxs[i];
					solidTileData[ast.row][ast.col] = 1;
				}

				Crafty.bind("EnterFrame", enterFrameFunction);
				sunShine.tween({alpha:1},1000);
			}




		},1000,10);
	};
	
	
});

function convertLevelTo2D(levelArray, numRows) {
	var numCols = levelArray.length / numRows;
	if (numCols !== Math.round(numCols)) {
		console.log('Error: level data and suggested row number do not match.');
	}
	var levelMatrix = new Array();
	//numCols = 3;
	//levelArray = [0, 1, 2, 3, 4, 5, 6, 7, 8];
	for (var rowIdx = 0; rowIdx < numRows; rowIdx++) {
		levelMatrix[rowIdx] = levelArray.splice(0, numCols);
	}
	return levelMatrix.reverse();
}