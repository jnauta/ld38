params = {
	playerStartPhi: -Math.PI / 2,
	playerStartRFromSurface: 200,
	playerAcc: 0.0002,
	dPort: 100,

	startAngleSun: Math.PI,

	maxTemp : 100,
	burnTemp : 90,
	chillMaxTemp : 70,
	chillMinTemp : 30,
	freezeTemp : 10,
	minTemp : 0,
	followTemp: 0.04,

	gravity: 0.15,
	vrDrag: 0.99,
	vaDrag: 0.05,
	maxVa: 2.5,
	maxVr: 5.0,
	jumpSpeed: 4.65,
	jumpSpWarm: 5.4,
	jumpSpHot: 6.2,
	nudgeStep: 0.0001,

	rumbleweedSpeedFactor: 0.003,
	rumbleRollFactor: 5000,
	wyrmSpeedFactor: 0.0005,

	surfaceRadius: 1000,
	layerThickness: 30,
	zLayers : {
		player: 1000,
		blocks: 500,
		crystal: 600,
	},

	winText: "You made it!<br>Time to fly home.",
}