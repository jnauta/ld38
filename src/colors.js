
mycolors = {
   
    background : '#878787', //'#C7B66D', //#D3BDDB',

    planet : '#9a7742',//'#000e69',
    tile1: '#8d4d1f',

   
    
    title: '#6E3E08',
    titleshadow: '#ffff00ff',    
    
    buttontext: '#ffffff',
    buttontextshadow: '#003300',
    buttontexthover: '#ffcccc',
    buttontexthovershadow: '#FFFFFF',

    popupBg: '#C4AE56',
    popupText: '#302A12',
    popupBorder: '#302A12',

    popupButtonBg: '#f4AE56',
    popupButtonText: '#302A12',
    popupButtonBorder: '#302A12',
}