Crafty.c('KeyControls', {
	init: function() {
		this.goingLeft = false;
		this.goingRight = false;
		this.jumping = false;
		this.bind('KeyDown', function(keyEvent) {
			if(this.playing){
				if (keyEvent.key === this.up && !this.jumping) { // jumpSlackFrames is reset in enterFrame
					//Crafty.audio.play('jump',1,1);
					this.jumping = true;
					this.changeState('Jump');
					var thermoJump = params.jumpSpeed;
					if (this.thermoLevel === 3) {
						thermoJump = params.jumpSpWarm;
					} else if (this.thermoLevel < 3) {
						thermoJump = params.jumpSpHot;
					}
					this.vr = thermoJump;//params.jumpSpeed + 0.4 * (3 - this.thermoLevel);
					// if(!mutesound){Crafty.audio.play('jump0',1,1);}
					//if(this.state && this.state !== "jumping"){this.jump();}
				} else if (keyEvent.key === this.right ) {
					//if(!this.jumping){this.walk()};
					this.goingRight = true;
					if(!this.jumping){this.changeState('Walk');}
					this.unflip('X');
					this.teleporter.unflip('X');
				} else if (keyEvent.key === this.left ) {
					//if(!this.jumping){this.walk()};
					this.goingLeft = true;
					if(!this.jumping){this.changeState('Walk');}
					this.flip('X');
					this.teleporter.flip('X');
				} else if (keyEvent.key === this.down) {
					if (this.teleportable && Game.tportCooldown === 0) {
						this.teleporter.animate('Send',3);
						if (this._flipX) {
							this.setPosition(this.radius, this.angle - params.dPort / this.radius);
							Game.tportCooldown = 100;							
						} else {
							this.setPosition(this.radius, this.angle + params.dPort / this.radius);
							Game.tportCooldown = 100;
						}
					}
				}
			}
		});
		this.bind('KeyUp', function(keyEvent) {
			if (keyEvent.key === this.up) {
			} else if (keyEvent.key === this.right) {
				this.goingRight = false;
				if(!this.jumping){this.changeState('Stand');}
			} else if (keyEvent.key === this.down) {

			} else if (keyEvent.key === this.left) {
				this.goingLeft = false;
				if(!this.jumping){this.changeState('Stand');}
			}
		});
	},
	_KeyControls: function(left, right, up, down) {
		this.left = left;
		this.right = right;
		this.up = up;
		this.down = down;
		return this;
	}
});