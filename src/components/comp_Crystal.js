Crafty.c("Crystal", {
	init: function() {
		this.requires("2D, Canvas, crystal, Crystal, Polar,Collision, ");
		
		
		this.z = params.zLayers["crystal"];
		// this.alpha = 1;
		this.activated = false; 
		
		
	},

	_Crystal : function(r, phi, idx){
		this.origin('top right');
		this.rotation = phi*utility.rad2deg
		this._Polar(r, phi);
		this.idx = idx;
		return this;
	},



	activate: function(){
		this.activated = true;
		console.log('activate crystal');
		this.sprite(1,0);			
	},

	deactivate: function(){
		this.activated = false;
		this.sprite(0,0);
	},
});




