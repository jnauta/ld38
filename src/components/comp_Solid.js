Crafty.c("Solid", {
	init: function() {
		this.requires("2D, Canvas, Collision");
		
		this.color = mycolors.tile1;
		this.z = params.zLayers["blocks"];
		this.imageBg = new Image();                                          
        	      	
		this.imageBg.src = imageMap["planetDirt"];
		this.ctx = this._drawLayer.context;
		
		this.patternBg = this.ctx.createPattern(this.imageBg, 'repeat'); 
		
		
		
	},

	_Solid : function(anglePos, radiusPos, type){
		this.type = type;
		
		// else if (type === "painful"){this.bind("Draw", this._drawPainful);}
		
		this.ready = true; // needed for drawing
		this.radiusPos = radiusPos; // sort of j
		this.anglePos = anglePos; // sort of i
		xPositions = [Math.cos(this.anglePos*angleStepRad) * (this.radiusPos*params.layerThickness + params.surfaceRadius), 
					  Math.cos( (this.anglePos + 1)*angleStepRad) * (this.radiusPos*params.layerThickness + params.surfaceRadius), 
					  Math.cos( (this.anglePos + 1)*angleStepRad) * ((this.radiusPos+1)*params.layerThickness + params.surfaceRadius), 
					  Math.cos( (this.anglePos)*angleStepRad) * ((this.radiusPos+1)*params.layerThickness + params.surfaceRadius)];
		minX = Math.min(xPositions[0],xPositions[1], xPositions[2],xPositions[3])-params.layerThickness/4;					  
		maxX = Math.max(xPositions[0],xPositions[1], xPositions[2],xPositions[3])+params.layerThickness/4;					  
		this.x = minX;
		this.w = maxX-minX;
		yPositions = [Math.sin(this.anglePos*angleStepRad) * (this.radiusPos*params.layerThickness + params.surfaceRadius), 
					  Math.sin( (this.anglePos + 1)*angleStepRad) * (this.radiusPos*params.layerThickness + params.surfaceRadius), 
					  Math.sin( (this.anglePos + 1)*angleStepRad) * ((this.radiusPos+1)*params.layerThickness + params.surfaceRadius), 
					  Math.sin( (this.anglePos)*angleStepRad) * ((this.radiusPos+1)*params.layerThickness + params.surfaceRadius)];
		minY = Math.min(yPositions[0],yPositions[1], yPositions[2],yPositions[3])-params.layerThickness/4;					  
		maxY = Math.max(yPositions[0],yPositions[1], yPositions[2],yPositions[3])+params.layerThickness/4;					  
		this.y = minY;
		this.h = maxY-minY;

		this.imageBg = new Image();                                          
        	      	
		if(this.type === "normal"){
			this.imageBg.src = imageMap["planetDirt"];
		}else if (this.type === "painful"){
			this.imageBg.src = imageMap["planetPainful"];
		}else if (this.type === "lava"){
			this.imageBg.src = imageMap["planetLava2"];	
		}
		// this.imageBg.rotate(this.w/2, this.h/2, 30);

		this.angleLeft = (this.anglePos)*angleStepRad;
		this.angleRight = (this.anglePos+1)*angleStepRad;
		this.radiusSmall = params.surfaceRadius + (this.radiusPos)*params.layerThickness ;
		this.radiusBig = params.surfaceRadius + (this.radiusPos + 1)*params.layerThickness;

		if(this.type === 'painful' || this.type === 'crystal'){
			this.angleLeft += angleStepRad/16;
			this.angleRight -= angleStepRad/16;
			this.radiusSmall += params.layerThickness/16; 
			this.radiusBig -= params.layerThickness/16;
		}


		this.ctx = this._drawLayer.context;	
		this.patternBg = this.ctx.createPattern(this.imageBg, 'repeat');		
		this.bind("Draw", this._drawNormal);
		

		return this;
	},

	_drawNormal: function(e) {
		if (!this.ready) return;

		ctx = this.ctx;
		
		if(!this.patternBg){this.patternBg = ctx.createPattern(this.imageBg,'repeat'); }
		
		if(!this.imageBg){ console.log('no imageBg')};
		if(!this.patternBg){ console.log('no patternBg... type of block: ' + this.type );
		console.log(this.imageBg)};

		
		ctx.save();
		
		
		ctx.lineWidth = 1;

		if (this.type === 'painful') {
			ctx.setLineDash([1, 3]);
			ctx.lineWidth = 5;
			ctx.strokeStyle = '#366925'; // color of edge of cactus
			ctx.fillStyle = '#366925'; //backup color for inside
			ctx.fillStyle = this.patternBg;
		}else{
			ctx.setLineDash([3, 3]);
			ctx.fillStyle = '#9a7742';
			ctx.strokeStyle = '#9a7742'; // if no patternBg :( 
			ctx.strokeStyle = this.patternBg;
			// ctx.fillStyle = this.patternBg;
			ctx.fillStyle = this.patternBg;
		}

		ctx.beginPath();

		ctx.arc(
			0, 0,
			this.radiusBig,
			this.angleLeft,
			this.angleRight
		);
		

		ctx.arc(
			0, 0,
			this.radiusSmall,
			this.angleRight,
			this.angleLeft,
			true
		);
		

		ctx.closePath();
		ctx.stroke();

		ctx.fill();
		ctx.restore();
	},

	
});




