Crafty.c("Rumbleweed", {
	init: function() {
		this.requires("2D, Canvas, rumbleweed,  Moving, Collision, ");
	},
	_Rumbleweed: function(r, phi, speed) {
		this.origin('center');
		this._Polar(r, phi);
		this.rotation = this.angle * utility.rad2deg;
		this.speed = this.va = params.rumbleweedSpeedFactor * speed;
		this.collision(10,0,20,0,30,10,30,20,20,30,10,30,0,20,0,10);
		

		// this.collision(Crafty.circle(0,0,10));
		return this;
	},
	ownVelocityUpdate: function() {
	},
	ownMove: function() {
		if (this.va > 0) {
			this.rotation += params.rumbleRollFactor * this.speed;
		} else if (this.va < 0) {
			this.rotation -= params.rumbleRollFactor * this.speed;
		}
	},
	moveCollisionTest: function() {
		if (this.polarCollision()) {
			if (this.va > 0) {
				this.va = -this.speed;
				this.flip('X');
			} else {
				this.va = this.speed;
				this.unflip('X');
			}
			return true;
		}
		return false;
	}
});