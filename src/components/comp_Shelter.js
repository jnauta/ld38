Crafty.c("Shelter", {
	init: function() {
	},
	// type = constant or average with sun
	_Shelter: function(startTemp, type) {
		this.T = startTemp;
		this.type = type ? type : "average";
		return this;
	},
	setTemp(temp){
		this.T = temp;
	}
});