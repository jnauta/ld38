Crafty.c("Sun", {
	init: function() {
		this.requires("2D, SunLayer, Tween, sunny");
		// this.bind("Draw", this._draw_me);		
		// this.ready = true; // needed for drawing
		
		// this.alpha = 0.9;
		this.time = params.startAngleSun;
		// this.color('#ffff00');
		
		
		this.z = 10000;
		this.origin(10,10);
		
		
	},
	
	_Sun : function(){
		
		
		return this;
	},

	updateTime: function(){
		this.time += 0.0012;
		this.x = 290 + Math.cos(this.time)*285;
		this.y = 290 + Math.sin(this.time)*285;
		
		this.rotation = this.time * utility.rad2deg;
		if(this.time > 2*Math.PI){
			this.time -= Math.PI*2;
		}
		
		// difference in angle of player and sun
		var difAngle = (this.time-player.angle) % (Math.PI*2);
		if(difAngle > Math.PI){
			difAngle -= Math.PI*2;			
		}else if(difAngle < - Math.PI){
			difAngle += Math.PI*2;			
		}
		
		var ratio = Math.cos(this.time-player.angle); // old, too long too hot or too cold
		
		var tempRatio = 1-Math.abs(difAngle)/Math.PI; // from 0 to 1
		// ratio = tempRatio;
		this.temp = 100*tempRatio;
		// console.log(this.temp);
		// color of sun
		hot0 = [255,222, 36]; // yellow
		mid0 = [255, 171, 36];// yellow less bright
		dark0 = [0,0,0]; // black
		// color close to sun. 
		hot1 = [255, 190, 0]; // light orange
		mid1 = [217, 113, 184];//[95,217,217]; // light blue
		dark1 = [0,0,0]; // black
		// color close to planet
		hot2 = [255, 0, 0];  //red
		mid2 = [15, 33, 135];  // dark blue
		dark2 = [0, 17, 143];  //dark blue
		// var tempSlider = tempRatio*2-1 // between -1 and 1
		var tempSlider = ratio;
		
		
		if(tempSlider > 0){ // tempSlider between -1 and 0			
			color0 = [tempSlider*hot0[0] + (1-tempSlider)*mid0[0],
					 tempSlider*hot0[1] + (1-tempSlider)*mid0[1],
					 tempSlider*hot0[2] + (1-tempSlider)*mid0[2]];
			color1 = [tempSlider*hot1[0] + (1-tempSlider)*mid1[0],
					 tempSlider*hot1[1] + (1-tempSlider)*mid1[1],
					 tempSlider*hot1[2] + (1-tempSlider)*mid1[2]];
			color2 = [tempSlider*hot2[0] + (1-tempSlider)*mid2[0],
					 tempSlider*hot2[1] + (1-tempSlider)*mid2[1],
					 tempSlider*hot2[2] + (1-tempSlider)*mid2[2]];
		}else{ // tempSlider between -1 and 1
			color0 = [-tempSlider*dark0[0] + (1+tempSlider)*mid0[0],
					 -tempSlider*dark0[1] + (1+tempSlider)*mid0[1],
					 -tempSlider*dark0[2] + (1+tempSlider)*mid0[2]];
			color1 = [-tempSlider*dark1[0] + (1+tempSlider)*mid1[0],
					 -tempSlider*dark1[1] + (1+tempSlider)*mid1[1],
					 -tempSlider*dark1[2] + (1+tempSlider)*mid1[2]];
			color2 = [-tempSlider*dark2[0] + (1+tempSlider)*mid2[0],
					 -tempSlider*dark2[1] + (1+tempSlider)*mid2[1],
					 -tempSlider*dark2[2] + (1+tempSlider)*mid2[2]];
		}
		
		// sun color, color close to sun, and color on other side
		colorRgb0 = "rgb(" + Math.floor(color0[0]) + ',' + Math.floor(color0[1]) +',' + Math.floor(color0[2]) + ")";		
		colorRgb1 = "rgb(" + Math.floor(color1[0]) + ',' + Math.floor(color1[1]) +',' + Math.floor(color1[2]) + ")";		
		colorRgb2 = "rgb(" + Math.floor(color2[0]) + ',' + Math.floor(color2[1]) +',' + Math.floor(color2[2]) + ")";		

		// calculate position of sun
		var sunx = 300 + Math.cos(this.time)*360;
		var suny = 300 + Math.sin(this.time)*360;
		
		Crafty.background(colorRgb0);
		Crafty.background('radial-gradient(circle farthest-corner at '+ sunx + 'px ' + suny + 'px , '+  colorRgb0 +' 0%, '+  colorRgb1 +' 20%,'+  colorRgb2 +'  100%)');

	},

    _draw_me: function (e) {
        if (!this.ready) return;
  //       var ctx = this._drawLayer.context;
		// ctx.save();
		// ctx.strokeStyle = "green";
		// ctx.fillStyle = this.color;

		// ctx.beginPath();

		// ctx.arc(
		//    e.pos._x + this.radius, e.pos._y + this.radius,
		//    this.radius,
		//    0,
		//    Math.PI*2
		// );


		// ctx.closePath();
		// ctx.stroke();
		// ctx.fill();
        
	}


	
});




