Crafty.c("Hopcat", {
	init: function() {
		this.requires("2D, Canvas, cathopper, SpriteAnimation, Moving, Collision, Gravipull");//.attr({w: 30, h: 30});

	},
	_Hopcat: function(r, phi, hopSpeed) {
		this.origin('center');
		this._Polar(r, phi);
		this.rotation = this.angle * utility.rad2deg;
		this.hopSpeed = hopSpeed;

		this.reel('Jump',500,0,0,2);
		this.animate('Jump',-1);

		return this;
	},
	moveCollisionTest: function() {
		if (this.polarCollision()) {
			this.vr = this.hopSpeed;
		}
		return false;
	},
	ownVelocityUpdate: function() {

	},
	ownMove: function() {

	}
});