Crafty.c("Moving", {
	init: function(){
		this.requires("2D, OriginCoordinates, Polar");
		this.vr = 0.0;
		this.va = 0.0;
		this.aVa = 0.4 * utility.deg2rad;
	},
	velocityUpdate: function() {
		this.ownVelocityUpdate();
		if (this.has('Gravipull')) {
			this.vr = Math.max(this.vr - params.gravity, -params.maxVr);
		}
	},

	moveUpdate: function() {
		this.ownMove();
		if (this.va !== 0) {
			this.prevAngle = this.angle;
			this.angle += this.va;
			this.updateXY();
			if (this.moveCollisionTest('a')) {
				this.angle = this.prevAngle;
				this.updateXY();
			}
		}
		
		if (this.vr !== 0) {
			this.rotation = (this.angle + Math.PI / 2) * utility.rad2deg;
			this.prevRadius = this.radius;
			this.radius += this.vr;
			this.updateXY();
			var contact = this.moveCollisionTest('r');
			if (contact) {
					this.radius = this.prevRadius;
					this.updateXY();
			} else {
				// update angular velocity to sort of conserve angular momentum: va * radius^2 == constant
				//this.va = this.va * (this.prevRadius * this.prevRadius) / (this.radius * this.radius);
				
				
			}
		}
	},
	
});