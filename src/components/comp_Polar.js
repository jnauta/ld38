Crafty.c("Polar", {
	init: function() {
		this.radius = 0;//params.surfaceRadius + params.playerStartRFromSurface;
		this.angle = 0;//params.playerStartPhi;
	},

	_Polar: function(radius, angle) {
		this.setPosition(radius, angle);
		return this;
	},

	updateXY: function() {		
		this.x = Math.cos(this.angle) * this.radius - this._origin.x;
		this.y = Math.sin(this.angle) * this.radius - this._origin.y;
	},
	
	setPosition: function(radius, angle) {
		this.radius = radius;
		this.angle = angle;
		this.updateXY();
	},
	polarCollision: function() {
		var minR = this.radius - this._h / 2;
		var maxR = this.radius + this._h / 2;
		var minPhi = this.angle - this._w / (3 * this.radius);//params.surfaceRadius);
		var maxPhi = this.angle + this._w / (3 * this.radius);//params.surfaceRadius);
		var minRow = Math.floor((minR - params.surfaceRadius) / params.layerThickness);
		var maxRow = Math.floor((maxR - params.surfaceRadius) / params.layerThickness);
		minPhi = utility.normalizeAngle(minPhi);
		var minCol = Math.floor(minPhi / angleStepRad);
		maxPhi = utility.normalizeAngle(maxPhi);
		var maxCol = Math.floor(maxPhi / angleStepRad);

		// Fix edge cases, piecewise collision checks may be possible
		minRow = Math.max(0, minRow); 
		maxRow = Math.max(0, Math.min(maxRow, solidTileData.length - 1));
		var minCol2 = null, maxCol2 = null;
		if (minCol > maxCol) {
			minCol2 = minCol;
			minCol = 0;
		}
		if (minRow < solidTileData.length) {
			for (var rowIdx = minRow; rowIdx <= maxRow; ++rowIdx) {
				for (var colIdx = minCol; colIdx <= maxCol; ++colIdx) {
					var tileType = solidTileData[rowIdx][colIdx];
					if (tileType === 1
						|| this.has("Rumbleweed") && (tileType === 13 || tileType === 6)) {
						return {row: rowIdx, col: colIdx};
					}

					var tileNum = solidTileData[rowIdx][colIdx];
					if (tileNum === 13 || tileNum === 23) {
						if (this === player) {
							if (tileNum === 13){
								this.death('spikes');
							}else{
								this.death('lava');
							}
						}
						return false;
					}
				}
				// possibly check other pieces on [0, 2PI]-boundary
				if (minCol2 !== null) {
					for (var colIdx = minCol2; colIdx <= solidTileData[0].length; ++colIdx) {
						if (solidTileData[rowIdx][colIdx] === 1) {
							return {row: rowIdx, col: colIdx};
						}
					}
				}
			}
		}
		return false;
	}
});