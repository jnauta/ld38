Crafty.c("Planet", {
	init: function() {
		this.requires("2D, Canvas, Collision");
		
		this.ready = true; // needed for drawing
		this.color = mycolors.planet;
		// this.alpha = 0.9;
		this.ctx = this._drawLayer.context;
		this.imageBg = new Image();                                          
		this.imageBg.src = imageMap["planetDirt"];
		this.patternBg = this.ctx.createPattern(this.imageBg, 'repeat');
		
		
	},
	
	_Planet : function(radius){
		
		this.radius = radius;		
		this.w = this.h = this.radius*2;
		this.x = -this.radius;
		this.y = -this.radius;
		this.ctx = this._drawLayer.context;
		this.imageBg = new Image();                                          
		this.imageBg.src = imageMap["planetDirt"];
		this.patternBg = this.ctx.createPattern(this.imageBg, 'repeat');
		
		this.bind("Draw", this._draw_me);		
		
		return this;
	},

    _draw_me: function (e) {
        if (!this.ready) return;
        var ctx = this._drawLayer.context;
		ctx.save();
		// ctx.strokeStyle = "green";
		ctx.fillStyle = this.color;
		// this.imageBg = new Image();                                          
		this.imageBg.src = imageMap["planetDirt"];
		this.patternBg = ctx.createPattern(this.imageBg, 'repeat');
		ctx.fillStyle = this.patternBg;

		ctx.beginPath();

		ctx.arc(
		   e.pos._x + this.radius, e.pos._y + this.radius,
		   this.radius,
		   0,
		   Math.PI*2
		);


		ctx.closePath();
		// ctx.stroke();
		ctx.fill();
		ctx.restore();
        
	}


	
});




