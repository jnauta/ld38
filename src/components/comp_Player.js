Crafty.c("Player", {
	init: function() {
		this.requires("2D, Canvas, player, SpriteAnimation, KeyControls, Moving, Collision, Tween, Delay, Gravipull");
		
		this.life = this.maxLife = 50;
		this.collision([4,20,26,20, 20,0,10,0]);
		this.temp = 50;
		this.attr({x:0,y:0, z:params.zLayers["player"]});
		

		this.maxTemp = params.maxTemp;
		this.burnTemp = params.burnTemp;
		this.chillMaxTemp = params.chillMaxTemp;
		this.chillMinTemp = params.chillMinTemp;
		this.freezeTemp = params.freezeTemp;
		this.minTemp = params.minTemp;
		
		this.lifeBgDiv = document.getElementById('round-world-viewBgLeft');
		this.lifeDiv = document.getElementById('round-world-viewColorLeft');
		this.thermoBgDiv = document.getElementById('round-world-viewBgRight');
		this.thermoDiv = document.getElementById('round-world-viewColorRight');
		
		
		// for resetting :
		
		this.thermoLevel = 3;
		this.lifeDiv.style.backgroundImage =  "url('assets/images/worldviewColorLeft.png')";
		this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight3.png')";
		this.lifeBgDiv.style.height = '0px';
		this.playing = true;
		this.teleportable = false;

	},

	_Player: function(r, phi, acceleration) {
		// near crystals
		// r = 1390;
		// phi = 5.74;
		// ufo?


		// right of start
		// r = 1200;
		// phi = 4.4;
		// west of firelake
		// r = 1500;
		// phi = 5.2 + Math.PI;
		// east of firelake
		// r = 1150;
		// phi = 6.2 + Math.PI;
		// firelake
		// r = 1700;
		// phi =  5.7 + Math.PI; 
		// end
		// phi = 4.0;
		// r = 1250;
		this.aVa = acceleration;
		this.origin('center');
		for(var i = 1;i<=5 ; i++){
			this.reel('Stand'+i,5000,0,i-1,1);
			this.reel('Jump'+i,5000,3,i-1,1);
			this.reel('Fall'+i,5000,4,i-1,1);
			this.reel('Walk'+i,500,[[1,i-1],[0,i-1], [2,i-1],[0,i-1]]);
		}
		this.state = 'Stand';
		this.animate('Stand3',-1);
		this._Polar(r, phi);

		this.teleporter = Crafty.e('2D,Canvas, teleporter, Tween,SpriteAnimation').attr({visible: false, x: this.x, y: this.y-15, z:this.z+1});
		this.teleporter.reel('Send',200,0,0,4).reel('Silent',1000,3,0,1).animate('Silent',-1);

		this.attach(this.teleporter);


		return this;
	},

	ownVelocityUpdate: function() {
		if(this.playing){
			if (this.goingRight) {
				this.va += this.aVa;
			}
			if (this.goingLeft) {
				this.va -= this.aVa;
			}
			// max/min angular speed for player and apply drag force
			if (this.va < 0) {
				this.va = Math.max(-params.maxVa / this.radius, Math.min(0.0, this.va + params.vaDrag / this.radius));
			} else if (this.va > 0) {
				this.va = Math.min(params.maxVa / this.radius, Math.max(0.0, this.va - params.vaDrag / this.radius));
			}
		}

	},

	ownMove: function() {
		
	},

	moveCollisionTest: function(dim) {
		// test enemies first
		if (this.hit('Hopcat') ){
			this.death('hopcat');
		} else if (this.hit('Rumbleweed') ){
			this.death('rumbleweed');
		} else if (this.hit('wyrm') ){
			this.life -= 1;
			if (this.life <= 0) {
				this.life = 0;
				this.death('wyrm');
			}
			this.lifeBgDiv.style.height = ((this.maxLife-this.life)/this.maxLife*600) + 'px';
			this.lifeDiv.style.backgroundImage =  "url('assets/images/worldviewColorLeft2.png')";
			this.delay(function(){
				this.lifeDiv.style.backgroundImage =  "url('assets/images/worldviewColorLeft.png')";
			},150,0);
		}
			
		var coldTime = 0;
		var spawnTime = 0;
		var freezeRoomTime = 0;
		var ledgeEarly = false;

		var areaHits = this.hit('Area');
		if (areaHits) {
			for (var i = 0; i < areaHits.length; i++) {
				var area = areaHits[i].obj;
				
				switch(area.name) {
				    case 'win':
				    	if(!this.win){
					        player.win = true;
					        infoDiv.style.visibility = "visible";
					        infoDiv.innerHTML = params.winText; 
					        player.playing = false;
					        player.tween({radius: rocketPolars.r-19, phi: rocketPolars.phi},2000);
					        player.delay(function(){

					        	this.visible = false;
					        	// i dont know how to stop player from moving! luckily it bumps against rock
					        	// player.va = 0;
					        	// player.vr = 0;
					        	// player.goingRight = false;
					        	// player.goingLeft = false;
					        	// player.removeComponent('Moving');

					        	
					        	rocket.tween({x:rocket._x-300, y: rocket._y - 400},1000, 'easeInQuad');
					        	
					        },2000,0);
					        
					    }
				        break;
				    case 'startspawn1':
				    	spawner1.startSpawn(180);
				    	Crafty.bind('EnterFrame', function() {
				    		++spawnTime;
				    		if (spawnTime === 1) {
				      		infoUpdateText('At night, the road ahead is blocked by firesplats.', 4000);
				    		} else if (spawnTime === 300) {
				      		infoUpdateText('The only way to get through the night<br>is by jumping on them to get warm.', 5000);
				    		}
				    	});
				        break;
				    case 'startspawn2':
				    	spawner2.startSpawn(150);

				        // code
				        break;
				    case 'teleporter':
				        infoUpdateText('Alien technology! Our chief alienalist suggests <br> pressing down arrow to see what happens.', 5000);
				        this.teleportable = true;
				        this.teleporter.visible = true;
				        break;
				    case 'gethot':
				        // code
				        if (this.temp < params.chillMaxTemp) {
									ledgeEarly = true;
	        				infoUpdateText('Hmm, that\'s a high ledge - looks like<br>you have to wait for the sun to warm you up...',4000);
	        			} else if (!ledgeEarly) {
	        				infoUpdateText('Good thing you\'re all warmed up, <br>so you can make these high jumps!', 4000);
	        			}
				        break;
				    case 'coldroom':
				    		if (coldTime === 0) {
						        Crafty.bind('EnterFrame',function(){
									coldTime += 1;
									if(coldTime === 1){
										infoUpdateText('Hmmm, sensors indicate that it\'s colder in here.<br>',3500);// ' could it have something to do with those crystals?', 5000);
									} else if(coldTime === 200){	
										infoUpdateText('But perhaps not cold enough...',4000);//That\'s nice during the day, but you better<br>get out of there before night falls...', 5000);
									}
								});
					      	}
				        break;
				    
				        
				    default:
				        // code block
				}
			}
		}

		var saves = this.hit('Save');
		if (saves) {
			var s = saves[0].obj;
			Crafty("Save").each(function(){
				if (s!== this) {
					this.animate("Off",-1);					
				}
			});
			if (!(s.isPlaying("On"))) {
				s.animate("On",-1);		
				// console.log('Save');		
				respawnPos = {r: s.radius, phi: s.angle, playerTemp : this.temp, sunTime: sunShine.time};
			}
			// need something like this but with phi and r:

			
			
		}

		var fsHits = this.hit('firesplat');
		if (fsHits) {
			for (var i = 0; i < fsHits.length; i++) {
			 	var fs = fsHits[i].obj;
				if (this.vr < 0) {
					this.vr = params.jumpSpeed;
					Game.tempBuff += 10;
					fs.destr();
				} else {
					this.death('firesplat');
				}
			}
		}
		var crystalHit = this.hit('Crystal');
		if (crystalHit) {
			var crystal = crystalHit[0].obj;
			if (crystal.idx === Game.crystalCounter + 1) {
				++Game.crystalCounter;
				Crafty.audio.play('crystalon', 1,0.1);	
				console.log(Game.crystalCounter);
				crystal.activate();
				if (Game.crystalCounter === crystals.length) {
					for (var ir = 0; ir < iceRooms.length; ir++) {
					 	iceRooms[ir].setTemp(0);
					 	
					} //crystalRoom.temperature = 30;

					if (freezeRoomTime === 0) {
				        Crafty.bind('EnterFrame',function(){
							freezeRoomTime += 1;
							if(freezeRoomTime === 1){
								infoUpdateText('The crystals resonate!',2500);
							} else if(freezeRoomTime === 200){	
								infoUpdateText('It\'s getting very cold there. <br>You\'d better move on as soon as possible!', 5000);
							}
						});
				    }

				}
			} else if (crystal.idx !== Game.crystalCounter && Game.crystalCounter < 3) {
				
				if(!crystal.activated){
					Game.crystalCounter = 0;
					Crafty.audio.play('crystaloff', 1,0.1);	
					crystal.activate();
					this.delay(function(){
						Crafty('Crystal').each(function(){this.deactivate();});
						
						
					},700,0);
			}
				// disable crystals; 
			}
		}

		var collision = this.polarCollision(true);
		// change player position slightly in case of sidewall collision
		if (collision) {	
																																// player angle is to player center
			var playerCol = utility.pol2tiledCR(this.radius, utility.normalizeAngle(this.angle));
			var dCol = collision.col - playerCol.col;
			var tilePolars = utility.tiled2pol(collision.col, collision.row, true);
			var nudge = false;
			if (dCol === 1 || dCol < -1) { // account for edge case 0-2PI
				var playerPhi = utility.normalizeAngle(this.angle + angleStepRad / 2);
				if (tilePolars.phi - playerPhi < params.nudgeStep) { // compare right side of player
					this.angle -= params.nudgeStep;
					nudge = true;
				}
			} else if (dCol === -1 || dCol > 1) { // account for edge case 0-2PI
				var playerPhi = utility.normalizeAngle(this.angle - angleStepRad / 2)
				if (tilePolars.phi + angleStepRad - playerPhi > -params.nudgeStep) { // compare left side of player
					this.angle += params.nudgeStep;
					nudge = true;
				}
			}
			if (dim === "a") {
				this.va = 0;
			}
			if (dim === "r") {
				if (this.vr < 0 && nudge === false) {
					this.vr = 0;
					this.jumping = false;
					// this.changeState('Walk');

				} else if (this.vr > 0) {
					this.vr = 0;
				}
			}
			if(this.state === "Fall"){
				this.changeState("Stand");
				// if(!Crafty.audio.isPlaying('hit')){
				// 	Crafty.audio.play('hit',1,1);	
				// }
			}
		} 
		// else {
		// 	if(this.has('Player') && this.vr < 0){this.changeState('Fall'); console.log('falllinnggg')}
		// }
		return collision;
	},

	checkWin: function() {
		if (this.angle > 2 * Math.PI) {
			// console.log('You win!');
		}
	},

	changeState: function(state){
			
		if(this.state != state ){
			// console.log(this.state + ' to ' + state);
			this.state = state;
			this.animate(state+this.thermoLevel,-1);

		}
	},

	changeColor: function(){		
		this.animate(this.state+this.thermoLevel,-1);		
	},

	lifeUpdate: function(dlife, cause){
		if(this.playing){
			this.life += dlife;
			if(this.life> this.maxLife){
				this.life = this.maxLife;

			}
			if(this.life <= 0){
				this.life = 0;
				this.death(cause);

			}

			this.lifeBgDiv.style.height = ((this.maxLife-this.life)/this.maxLife*600) + 'px';
			this.lifeDiv.style.backgroundImage =  "url('assets/images/worldviewColorLeft2.png')";
			this.delay(function(){
				this.lifeDiv.style.backgroundImage =  "url('assets/images/worldviewColorLeft.png')";
			},150,0);
		}


	},

	bodyHeatUpdate: function(temperature){
		if(this.playing){
			var shelters = this.hit("Shelter");
			if (shelters) {
				var shelter = shelters[0].obj;
				if (shelter.type === "constant") {
					temperature = shelter.T;
				} else if (shelter.type === "average") {
					temperature = (temperature + shelter.T) / 2
				}
			}
			temperature += Game.tempBuff;
			if(this.temp < temperature){
				this.temp += params.followTemp *(temperature-this.temp);
			}
			else if (this.temp > temperature){
				this.temp += params.followTemp *(temperature-this.temp);
			}
			
			if(this.temp > this.burnTemp){
				if(this.thermoLevel != 1){
					this.thermoLevel = 1;
					this.changeColor();
					
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight1.png')";
				}
				this.burnTimer += 1;
				this.thermoColor = mycolors.burn;
				if(this.burnTimer === 10){
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight0.png')";	
					

				}
				if(this.burnTimer > 20){
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight1.png')";
					this.lifeUpdate(-4, 'temp');
					// Crafty.audio.play('toohot',1,0.2);
					this.burnTimer = 0;
					this.changeColor();
				}
				// console.log('too hot');
			}else if(this.temp > this.chillMaxTemp){
				if(this.thermoLevel != 2){
					this.thermoLevel = 2;
					this.burnTimer = 0;
					this.changeColor();
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight2.png')";
				}
			}else if(this.temp > this.chillMinTemp){
				if(this.thermoLevel != 3){
					this.thermoLevel = 3;
					this.changeColor();
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight3.png')";
				}
			}else if(this.temp > this.freezeTemp){
				if(this.thermoLevel != 4){
					this.thermoLevel = 4;
					this.changeColor();
					this.freezeTimer = 0;
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight4.png')";
				}
			}else if(this.temp < this.freezeTemp){	
				if(this.thermoLevel != 5){
					this.thermoLevel = 5;
					this.changeColor();
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight5.png')";
				}		
				this.freezeTimer += 1;
				this.thermoColor = mycolors.freeze;
				if(this.freezeTimer === 10){
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight6.png')";
				}else if(this.freezeTimer > 20){
					this.thermoDiv.style.backgroundImage = "url('assets/images/worldviewColorRight5.png')";
					this.lifeUpdate(-4, 'temp');
					// Crafty.audio.play('toocold',1,0.2);
					this.freezeTimer = 0;
				}
				// console.log('too cold');
			}

			// console.log('player temp: ' + this.temp +  ', sun temp: ' + temperature );
			var heightThermo =  Math.floor((this.maxTemp-this.temp)/this.maxTemp*600); 
			// console.log(heightThermo);
			this.thermoBgDiv.style.height = heightThermo + 'px';
			// this.thermoBgDiv.style.top = (150-heightThermo) + 'px';
			// this.thermoBgDiv.style.backgroundColor = this.thermoColor;
			// console.log('updatingBodyHeat: ' + this.temp);
		}

	},

	death: function(type){
		if (this.playing) {
			this.tween({w:0,h:0,x:this.x, y:this.y, alpha:0, },500);
			this.teleporter.tween({w:0,h:0,alpha:0, },500);
			this.delay(function(){
				resetLevel();
			},500,0);
			Crafty.audio.play('death',1,0.1);	
			this.playing = false;
			
			switch(type) {
			    case 'spikes':
			        var text = 'Ouch, these plants sting.';
			        break;
			    case 'temp':
			        var text = 'I need to keep an eye on my temperature';
			        break;
			    case 'hopcat':
			        var text = 'Those catlike creatures bite!';
			        break;
			    case 'firesplat':
			        var text = 'Yaiks, it got me.';
			        break;
			    case 'lava':
			        var text = 'Ok, I should not fall into the lava.';
			        break;
			    case 'rumbleweed':
			        var text = "They look so cute, but they aren't!"; 
			        break;
			    case 'wyrm':
			        var text = 'Yaiks, it got me.';
			        break;
			    
			    
			        
			    default:
			        // code block
			}

			infoUpdateText(text, 4000);	

		}

	},
});