Crafty.c("Firesplat", {
	init: function() {
		this.requires("2D, Canvas, Moving, Collision,firesplat, SpriteAnimation").attr({w: 30, h: 30});
	},
	_Firesplat: function(r, phi, speed, kind) {
		// this.addComponent("firesplat");
		// this.addComponent("SpriteAnimation");
		this.reel('Fly',500,0,0,4).animate('Fly',-1);
		this._Polar(r, phi);
		this.rotation = this.angle * utility.rad2deg;
		this.speed = speed;
		this.va = 0;
		this.orig = utility.pol2tiledCR(r, phi);
		this.dest = this.nextDest(this.orig);
		this.nowRC = utility.pol2tiledCR(this.radius, this.angle);
		this.nowXY = utility.pol2car(this.radius, this.angle);
		this.dA = 0;
		this.dR = 0;
		this.accA = 0;
		this.accR = 0;
		return this;
	},
	moveCollisionTest: function() {
		return false;
	},
	ownVelocityUpdate: function() {
		var destPol = utility.tiled2pol(this.dest.col, this.dest.row, true);
		this.dA = destPol.phi - this.angle;
		this.dR = destPol.r - this.radius;
		this.accA = 0.004 * this.dA;
		this.va = 0.9 * (this.va + this.accA);
		this.accR = 0.003 * this.dR;
		this.vr = 0.9 * (this.vr + this.accR);
	},
	ownMove: function() {
		if (Math.abs(this.dA) < 0.01 && Math.abs(this.dR) < 10) {//this.nowRC.col === this.dest.col && this.nowRC.row === this.dest.row) {
				this.oldOrig = this.orig;
				this.orig = this.dest;
				this.dest = this.nextDest(this.orig, this.oldOrig);	
		}
	},
	nextDest: function(orig, oldOrig) {
		var freeFields = new Array();
		for (var r = -1; r <= 1; ++r) {
			var tileRow = solidTileData[orig.row + r - 1];
			for (var c = -1; c <= 1; ++c) {
				var field = tileRow[orig.col + c];
				if (field !== 1 && field !== 13 && field !== 6) {
					if (!oldOrig || !(oldOrig.row === orig.row + r && oldOrig.col === orig.col + c)) {
						freeFields.push({col: orig.col + c, row: orig.row + r});
					}
				}
			}
		}
		if (freeFields.length === 0) {
			return oldOrig;
		}
		return freeFields[Math.floor(Math.random()* freeFields.length)];
	}
});