Crafty.c("Wyrm", {
	init: function() {
		this.requires("2D, Canvas, Moving, Collision, Color, SpriteAnimation").attr({w: 30, h: 30});
	},
	_Wyrm: function(r, phi, speed, kind, owner) {
		this.addComponent(kind);
		if(kind === "firesplat"){
			this.collision([0,10,30,10,30,20,0,20]);
		}else{
			this.collision([10,0,20,0,20,30,10,30]);
		}
		this.reel('Fly',500,0,0,4).animate('Fly',-1);
		this._Polar(r, phi);
		this.rotation = this.angle * utility.rad2deg;
		this.speed = speed;
		this.va = 0;
		this.orig = utility.pol2tiledCR(r, phi);
		this.dest = this.nextDest(this.orig);
		this.nowRC = utility.pol2tiledCR(this.radius, this.angle);
		this.nowXY = utility.pol2car(this.radius, this.angle);
		this.dA = 0;
		this.dR = 0;
		this.accA = 0;
		this.accR = 0;
		this.owner = owner;
		//this.origin('bottom left');
		return this;
	},
	moveCollisionTest: function() {
		return false;
	},
	ownVelocityUpdate: function() {

		this.destPol = utility.tiled2pol(this.dest.col, this.dest.row, true);
		this.dA = this.destPol.phi - this.angle;
		this.dR = this.destPol.r - this.radius + params.layerThickness;
		// var dC = this.dest.col - this.nowRC.col;
		// var dR = this.dest.row - this.nowRC.row;
		this.accA = 0.0002 * Math.sign(this.dA) + 0.0001 * this.dA;
		this.va = 0.85 * (this.va + this.accA);//0.02 * this.dA * this.speed;//* params.wyrmSpeedFactor
		this.accR = 0.3 * Math.sign(this.dR);
		this.vr = 0.85 * (this.vr + this.accR);//0.02 * this.dR * this.speed;

	},
	ownMove: function() {
		if (Math.abs(this.dA) < 0.001 && Math.abs(this.dR) < 2) {//this.nowRC.col === this.dest.col && this.nowRC.row === this.dest.row) {
			if (this.hit('Lava')) {
				var index = firewyrms.indexOf(this);
				if (index > -1) {
					firewyrms.splice(index, 1)
				}
				this.destr();
			}
			this.oldOrig = this.orig;
			this.orig = this.dest;
			this.dest = this.nextDest(this.orig, this.oldOrig);
			this.destPol = utility.tiled2pol(this.dest.col, this.dest.row, true);
			
			if(this.dest.col < this.orig.col){
				this.flip("X");					
			}else if (this.dest.col > this.orig.col){						
				this.unflip("X");
			}				
		}
	},
	nextDest: function(orig, oldOrig) {
		var freeFields = new Array();
		for (var r = -1; r <= 1; ++r) {
			var tileRow = solidTileData[orig.row + r]; 
			for (var c = -1; c <= 1; ++c) {
				if ((c === 0 || r === 0) && !(c === 0 && r === 0)) {
					var nextTile = solidTileData[orig.row + r][orig.col + c];
					// var tilePol = utility.tiled2pol(orig.col + c, orig.row + r, true);
					// var mark = Crafty.e("2D, Canvas, Color, Polar").color('black').attr({w: 10, h: 10, z: 10000})._Polar(tilePol.r, tilePol.phi);
					if (this.fieldFree(nextTile)) {
						var cSteps = 1, rSteps = 1;
						var nextC = orig.col + c;
						var nextR = orig.row + r;
						var nextTile = solidTileData[nextR][nextC];
						while (this.fieldFree(nextTile)){
							++cSteps;
							++rSteps;
							nextC = Math.min(level.width - 1, orig.col + c * cSteps);
							nextR = Math.min(level.height - 1, orig.row + r * rSteps);
							nextTile = solidTileData[nextR][nextC];
						}
						if (!oldOrig || !(oldOrig.row === orig.row + r * (rSteps - 1) && oldOrig.col === orig.col + c * (cSteps - 1))) {
							freeFields.push({col: orig.col + c * (cSteps - 1), row: orig.row + r * (rSteps - 1)});
						}
					}
				}
			}
		}
		if (freeFields.length === 0) {
			return oldOrig;
		}
		return freeFields[Math.floor(Math.random() * freeFields.length)];
	},
	fieldFree: function(field) {
		if (field !== 1 && field !== 13 && field !== 6) {
			return true;
		}
		return false;
	},
	destr: function() {
		if (this.owner) {
			this.owner.mourn(this);
		}
		this.destroy();
	}
});

Crafty.c("Spawner", {
	init: function() {},
	_Spawner: function(r, phi, maxNum, speed) {
		this.r = r;
		this.phi = phi;
		this.timer = 0;
		this.maxNum = maxNum;
		this.speed = speed;
		this.frameCount = 0;
		this.interval = 0;
		this.firewyrms = new Array();
		this.firing = false;

		return this;
	},
	startSpawn: function(interval) {
		if(!this.firing){
			this.interval = interval;
			this.firing = true;
		}
	},
	stopSpawn: function(){
		if(this.firing){
			this.firing = false;
			this.frameCount = 0;
			this.interval = 0;
		}
	},
	spawn: function() {
		if(this.firing){
			var sunAngle = sunShine.time % (2 * Math.PI);
			if (sunAngle < 0.05 * Math.PI) {
				this.stopSpawn();
			}

			if (this.firewyrms.length < this.maxNum) {
				if (this.interval !== 0 && this.frameCount === this.interval) {
					var fw = Crafty.e("Wyrm")._Wyrm(this.r, this.phi, this.speed, "firesplat", this).flip('X');
					this.firewyrms.push(fw);
					firewyrms.push(fw);
					//icewyrms.push(Crafty.e('Wyrm')._Wyrm(iwPolars.r, iwPolars.phi, iw.properties.speed, iw.properties.kind));
					this.frameCount = 0;
				}
			}
			if (this.frameCount < this.interval) {
				++this.frameCount;
			}
		}
	},
	// removes fw that is about to be destroyed.
	mourn: function(fw) {
		var index = this.firewyrms.indexOf(fw);
			if (index > -1) {
				this.firewyrms.splice(index, 1)
		}
	}
});