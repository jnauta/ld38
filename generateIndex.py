#!/usr/bin/python

import os
import re

indexFile = open('index.html', 'w')


header = """<!DOCTYPE html>
<html>
    <head>
        <title>Shelton's Detour</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
        <link rel="stylesheet" type="text/css" href="stylesheet.css" />
        <!-- font -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favoicon.png"> 
    <link href='http://fonts.googleapis.com/css?family=Ranga' rel='stylesheet' type='text/css'>
    <!--   <meta name="viewport"   content="width=device-width, user-scalable=no, initial-scale=1.0" /> -->
    <script src="node_modules/craftyjs/dist/crafty.js"></script>"""
footer = """    <script>
      window.addEventListener('load', Game.start);
    </script>
    
    <!--  Thumbnail voor Facebook enzo. ;) -->
    <link rel="image_src" href="scrshot.png"/>  
  </head>
  <body>
        <!--Add your own HTML!-->   
        
    <div id="canvascontainer">  
        <div id="round-world-viewBgLeft"></div>
        <div id="round-world-viewColorLeft"></div>
        <div id="round-world-viewBgRight"></div>
        <div id="round-world-viewColorRight"></div>
        <div id="round-world-view"></div>
      
      <div id="cr-stage">
        <div id="info"> </div>  
      </div>
    </div>
    <!--<div><p><h1>Game Title</h1></p></div>-->
    <div>
      <p>Made by <a href="http://jellenauta.com/games/">Jelle & Anneroos</a> with <a href="http://craftyjs.com">CraftyJS</a> 
             for <a href="http://www.ludumdare.com/compo/ludum-dare-38/?action=preview&uid=18490">Ludum Dare 38</a> with the theme 'Small World'.</p> 
      
    </div>
    
  </body>
</html>"""

try:
  jsRegex = re.compile('.*\.js');
  indexFile.write(header)
  for root, dirs, files in os.walk('./src/'):
    for name in files:
      indexFile.write('    <script src="');
      indexFile.write(os.path.join(root, name));
      indexFile.write('"></script>\n');
  for root, dirs, files in os.walk('./assets/'):
    for name in files:
      if jsRegex.match(name):
        indexFile.write('    <script src="');
        indexFile.write(os.path.join(root, name));
        indexFile.write('"></script>\n');

  indexFile.write(footer)

finally:
  indexFile.close()